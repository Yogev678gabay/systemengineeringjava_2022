package main;

import my_game.MyCharacter;
import my_game.Pokimon;
import my_game.MyCharacter;
import java.awt.event.KeyEvent;

import game.KeyboardListener;

public class MyKeyboardListener extends KeyboardListener {

	private MyContent myContent;

	public MyKeyboardListener() {
		super();
		myContent = (MyContent) this.content;
	}

	@Override
	public void directionalKeyPressed(Direction direction) {
		try {
			switch (direction) {
				case RIGHT:
					myContent.pokimon().setDirectionPolicy(Pokimon.Direction.RIGHT);
					myContent.pokeball().setDirectionPolicy(MyCharacter.Direction.LEFT);
					break;
				case LEFT:
					myContent.pokimon().setDirectionPolicy(Pokimon.Direction.LEFT);
					myContent.pokeball().setDirectionPolicy(MyCharacter.Direction.RIGHT);
					break;
				case UP:
					myContent.pokimon().setDirectionPolicy(Pokimon.Direction.UP);
					myContent.pokeball().setDirectionPolicy(MyCharacter.Direction.DOWN);
					myContent.pokimon().setRotation(myContent.pokimon().getRotation() + 20);
					break;
				case DOWN:
					myContent.pokimon().setDirectionPolicy(Pokimon.Direction.DOWN);
					myContent.pokeball().setDirectionPolicy(MyCharacter.Direction.UP);
					myContent.pokimon().setRotation(myContent.pokimon().getRotation() - 20);
					break;
			}
		} 
		catch (Exception e) {
			// do nothing
		}
	}

	@Override
	public void characterTyped(char c) {
		System.out.println("key character = '" + c + "'" + " pressed.");
	}
	@Override
	public void enterKeyPressed() {
		myContent.pokeball().setDirectionPolicy(MyCharacter.Direction.DOWN);
		System.out.println("enter key pressed.");
	}
	@Override
	public void backSpaceKeyPressed() {
		System.out.println("backSpace key pressed.");
	}
	@Override
	public void spaceKeyPressed() {
		myContent.pokeball().setDirectionPolicy(MyCharacter.Direction.RIGHT);
		System.out.println("space key pressed.");
	}
	public void otherKeyPressed(KeyEvent e) {
		System.out.println("other key pressed. type:" + e);
	}
}
